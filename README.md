# OpenML dataset: user-knowledge

https://www.openml.org/d/1508

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: UCI    
**Please cite**:  H. T. Kahraman, Sagiroglu, S., Colak, I., Developing intuitive knowledge classifier and modeling of users' domain dependent data in web, Knowledge Based Systems, vol. 37, pp. 283-295, 2013.   

* Title:  

User Knowledge Modeling Data Set 

* Abstract: 

It is the real dataset about the students' knowledge status about the subject of Electrical DC Machines. The dataset had been obtained from Ph.D. Thesis.

* Source:

-- Creators: Hamdi Tolga Kahraman (htolgakahraman '@' yahoo.com) 
-- Institution: Faculty of Technology, Department of Software Engineering, Karadeniz Technical University, Trabzon, Turkiye 
-- Creators: Ilhami Colak (icolak '@' gazi.edu.tr) 
-- Institution: Faculty of Technology, Department of Electrical and Electronics Engineering, Gazi University, Ankara, Turkiye 
-- Creators: Seref Sagiroglu (ss '@' gazi.edu.tr) 
-- Institution: Faculty of Technology, Department of Computer Engineering, Gazi University, Ankara, Turkiye 

-- Donor: undergraduate students of Department of Electrical Education of Gazi University in the 2009 semester 
-- Date: October, 2009


* Data Set Information:

-- The users' knowledge class were classified by the authors 
using intuitive knowledge classifier (a hybrid ML technique of k-NN and meta-heuristic exploring methods), k-nearest neighbor algorithm. See article for more details on how the users' data was collected and evaluated by the user modeling server. 

H. T. Kahraman, Sagiroglu, S., Colak, I., Developing intuitive knowledge classifier and modeling of users' domain dependent data in web, Knowledge Based Systems, vol. 37, pp. 283-295, 2013.


* Attribute Information:

STG (The degree of study time for goal object materails), (input value) 
SCG (The degree of repetition number of user for goal object materails) (input value) 
STR (The degree of study time of user for related objects with goal object) (input value) 
LPR (The exam performance of user for related objects with goal object) (input value) 
PEG (The exam performance of user for goal objects) (input value) 
UNS (The knowledge level of user) (target value)


* Relevant Papers:

1. H. T. Kahraman, Sagiroglu, S., Colak, I., Developing intuitive knowledge classifier and modeling of users' domain dependent data in web, 
Knowledge Based Systems, vol. 37, pp. 283-295, 2013. 
2. Kahraman, H. T. (2009). Designing and Application of Web-Based Adaptive Intelligent Education System. Gazi University Ph. D. Thesis, Turkey, 1-156.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1508) of an [OpenML dataset](https://www.openml.org/d/1508). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1508/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1508/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1508/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

